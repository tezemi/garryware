
CHAOS_GRAVITY = {}

CHAOS_GRAVITY.Enabled = false
CHAOS_GRAVITY.Name = "Chaos Gravity"

function CHAOS_GRAVITY:Enable()

    CHAOS_GRAVITY.Enabled = true

    timer.Create( "chaos-gravity-timer", 1, 0, function() 
    
        local ran1 = (math.random() * 600) + -(math.random() * 600);
        local ran2 = (math.random() * 600) + -(math.random() * 600);
        local ran3 = (math.random() * 600) + -(math.random() * 600);

        physenv.SetGravity( Vector( ran1, ran2, ran3 ) ) 

        for k, v in ipairs( ents.GetAll() ) do	

            local phys = v:GetPhysicsObject()
            if IsValid( phys ) then

                phys:Wake() 

            end
    
        end

    end )

end

function CHAOS_GRAVITY:Disable()

    CHAOS_GRAVITY.Enabled = false

    timer.Remove( "chaos-gravity-timer" )

    physenv.SetGravity( Vector( 0, 0, -600 ) )

    for k, v in ipairs( ents.GetAll() ) do	

        local phys = v:GetPhysicsObject()
        if IsValid( phys ) then

            phys:Wake() 

        end

    end

end

