
EXPLOSIVE_PROPS = {}

EXPLOSIVE_PROPS.Enabled = false
EXPLOSIVE_PROPS.Name = "Explosive Props"

function EXPLOSIVE_PROPS:Enable()

    EXPLOSIVE_PROPS.Enabled = true

    for k, v in ipairs( ents.GetAll() ) do	

        if (v:GetClass() == "prop_physics" || v:GetClass() == "prop_physics_multiplayer" || v:GetClass() == "prop_physics_respawnable") then

            local ent = ents.Create( "prop_physics" )
            ent:SetModel( "models/props_c17/oildrum001_explosive.mdl" )
            ent:SetPos( v:GetPos() )
            ent:Spawn()

            v:Remove()

        end

    end

end

function EXPLOSIVE_PROPS:Disable()

    EXPLOSIVE_PROPS.Enabled = false

end
