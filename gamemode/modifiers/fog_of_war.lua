
FOG_OF_WAR = {}

FOG_OF_WAR.Name = "Fog of War"

function FOG_OF_WAR:Enable()

    render.FogStart( 0 ) 
    render.FogEnd( 300 )
    render.FogMaxDensity( 1 )  

end

function FOG_OF_WAR:Disable()

end