
VAMPIRE_PLAYERS = {}

VAMPIRE_PLAYERS.Enabled = false
VAMPIRE_PLAYERS.Name = "Vampire Players"

function VAMPIRE_PLAYERS:Enable()

    VAMPIRE_PLAYERS.Enabled = true

end

function VAMPIRE_PLAYERS:Disable()

    VAMPIRE_PLAYERS.Enabled = false

end

hook.Add("EntityTakeDamage", "VAMPIRE_PLAYERS:EntityTakeDamage", function( target, dmg )

    if (!VAMPIRE_PLAYERS.Enabled) then return end

    if (target:IsPlayer() && dmg:GetAttacker():IsPlayer()) then

        dmg:GetAttacker():SetHealth(dmg:GetAttacker():Health() + dmg:GetDamage() / 2)

    end
    
end)