
PROP_RAIN = {}

PROP_RAIN.Enabled = false
PROP_RAIN.Name = "Explosive Barrel Rain"

local function RainProp()

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && math.random() < 0.25) then

            local ent = ents.Create( "prop_physics" )
            ent:SetModel( "models/props_c17/oildrum001_explosive.mdl" )
            ent:SetAngles( Angle( math.random() * 360, math.random() * 360, math.random() * 360 ) )
            ent:SetPos( ply:GetPos() + Vector( (math.random() * 650) - (math.random() * 650), (math.random() * 650) - (math.random() * 650), 1000 ) )
            ent:Spawn()

        end

    end

end

function PROP_RAIN:Enable()

    PROP_RAIN.Enabled = true

    timer.Create("prop-rain-timer", 0.25, 0, RainProp)

end

function PROP_RAIN:Disable()

    PROP_RAIN.Enabled = false

    timer.Remove("prop-rain-timer")

end

