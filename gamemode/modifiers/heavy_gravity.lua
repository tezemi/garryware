
HEAVY_GRAVITY = {}

HEAVY_GRAVITY.Enabled = false
HEAVY_GRAVITY.Name = "Heavy Gravity"

function HEAVY_GRAVITY:Enable()

    HEAVY_GRAVITY.Enabled = true
    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(1.25)

    end

end

function HEAVY_GRAVITY:Disable()

    HEAVY_GRAVITY.Enabled = false
    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(1)

    end

end

