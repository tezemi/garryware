
LOW_GRAVITY = {}

LOW_GRAVITY.Enabled = false
LOW_GRAVITY.Name = "Low Gravity"

function LOW_GRAVITY:Enable()

    LOW_GRAVITY.Enabled = true
    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(0.25)

    end

end

function LOW_GRAVITY:Disable()

    LOW_GRAVITY.Enabled = false
    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(1)

    end

end

