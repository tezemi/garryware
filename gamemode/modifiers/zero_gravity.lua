
ZERO_GRAVITY = {}

ZERO_GRAVITY.Enabled = false
ZERO_GRAVITY.Name = "Zero Gravity"

function ZERO_GRAVITY:Enable()

    ZERO_GRAVITY.Enabled = true

    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(0.15)

    end

    physenv.SetGravity( Vector( 0, 0, 1 ) ) 

    for k, v in ipairs( ents.GetAll() ) do	

        local phys = v:GetPhysicsObject()
        if IsValid( phys ) then

            phys:Wake() 

        end

    end

    timer.Create( "zero-gravity-timer", 1, 1, function() 
    
        physenv.SetGravity( Vector( 0, 0, 0 ) ) 

    end )

end

function ZERO_GRAVITY:Disable()

    ZERO_GRAVITY.Enabled = false

    timer.Remove( "zero-gravity-timer" )

    physenv.SetGravity( Vector( 0, 0, -600 ) )

    for k, v in ipairs( ents.GetAll() ) do	

        local phys = v:GetPhysicsObject()
        if IsValid( phys ) then

            phys:Wake() 

        end

    end

    for key, ply in pairs(player.GetAll()) do

        ply:SetGravity(1)

    end

end

