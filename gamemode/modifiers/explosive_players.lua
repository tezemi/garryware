
EXPLOSIVE_PLAYERS = {}

EXPLOSIVE_PLAYERS.Enabled = false
EXPLOSIVE_PLAYERS.Name = "Explosive Players"

function EXPLOSIVE_PLAYERS:Enable()

    EXPLOSIVE_PLAYERS.Enabled = true

end

function EXPLOSIVE_PLAYERS:Disable()

    EXPLOSIVE_PLAYERS.Enabled = false

end

hook.Add("PlayerDeath", "EXPLOSIVE_PLAYERS:PlayerDeath", function( victim, inflictor, attacker )

    if (!EXPLOSIVE_PLAYERS.Enabled) then return end

    local radius = 300

    local explode = ents.Create( "env_explosion" )
    explode:SetPos( victim:GetPos() + Vector( 0, 2, 0 ) )
    explode:SetOwner( victim )
	explode:SetKeyValue( "iMagnitude", "120" )
	explode:Fire( "Explode", 0, 0 )
    explode:EmitSound( "weapon_AWP.Single", 400, 400 )
    
end)