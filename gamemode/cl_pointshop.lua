include( "games/base.lua" )

concommand.Add( "gw_pointmenu", function( ply, cmd, args, str )

	local frame = vgui.Create("DFrame")
	frame:SetPos( ScrW() / 2, ScrH() / 2 )
	frame:SetSize( ScrW() / 2, ScrH() / 2)
	frame:SetTitle( "Point Shop" )
	frame:SetVisible(true)
	frame:SetDraggable(true)
	frame:ShowCloseButton(true)
	frame:MakePopup()

    local sheet = vgui.Create( "DPropertySheet", frame )
    sheet:Dock( FILL )

    local panel1 = vgui.Create( "DPanel", sheet )
    sheet:AddSheet( "Buy Modifier", panel1, "icon16/wand.png" )

    local panel2 = vgui.Create( "DPanel", sheet )
    sheet:AddSheet( "Buy Noisemaker", panel2, "icon16/sound.png" )

    local panel3 = vgui.Create( "DPanel", sheet )
    sheet:AddSheet( "Buy Weapons", panel3, "icon16/bomb.png" )

    local layout = vgui.Create( "DTileLayout", panel1 )
    layout:SetBaseSize( 32 )
    layout:Dock( FILL )
    layout:MakeDroppable( "pointshop_general" )
    
    for k, v in pairs( BASE.AvailableModifiers ) do

        local buyModButton = vgui.Create( "DButton", layout )
        buyModButton:SetText( v.Name )
        buyModButton:SetSize( 250, 30 )
        buyModButton.DoClick = function()
    
            RunConsoleCommand( "say", v.Name )
    
        end

        layout:Add( buyModButton )
        
    end  

    local noiseMakerLayout = vgui.Create( "DTileLayout", panel2 )
    noiseMakerLayout:SetBaseSize( 32 )
    noiseMakerLayout:Dock( FILL )
    noiseMakerLayout:MakeDroppable( "noisemaker_layout" )

    local buyNoisemakerButton = vgui.Create( "DButton", noiseMakerLayout )
    buyNoisemakerButton:SetText( "Buy Noisemaker" )
    buyNoisemakerButton:SetSize( 250, 30 )
    buyNoisemakerButton.DoClick = function()

        net.Start( "try-buy-noisemaker" )
        net.SendToServer()

    end

    noiseMakerLayout:Add( noiseMakerLayout )
    
end )