include( "shared.lua" )
include( "cl_scoreboard.lua" )
include( "cl_hud.lua" )

local DrawHunterHalos = false
local DrawTTTHalos = false
local HotbugDrawAllHalos = false
local Blind = false
local HotbugBuggedPlayer = nil
local TouchTheBallHalos = false
local DrawGhostOverlay = false

function GM:PreDrawHalos()

    if ( DrawHunterHalos ) then

        local playerTable = player.GetAll()
        for key, ply in pairs(playerTable) do

            if ( !ply:Alive() || ply:GetObserverMode() ~= OBS_MODE_NONE ) then

                playerTable[key] = nil

            end

        end

        halo.Add( playerTable, Color( 100, 255, 0 ), 1, 1, 1, false, true )

    end

    if ( DrawTTTHalos ) then

        local playerTable = player.GetAll()
        for key, ply in pairs(playerTable) do

            if ( !ply:Alive() || ply:GetObserverMode() ~= OBS_MODE_NONE ) then

                playerTable[key] = nil

            end

        end

        halo.Add( playerTable, Color( 255, 0, 0 ), 2, 2, 2, false, true )

    end

    if ( HotbugDrawAllHalos ) then

        local playerTable = player.GetAll()
        for key, ply in pairs(playerTable) do

            if ( !ply:Alive() || ply:GetObserverMode() ~= OBS_MODE_NONE ) then

                playerTable[key] = nil

            end

        end

        halo.Add( playerTable, Color( 255, 25, 25 ), 2, 2, 2, false, true )

    end

    if ( HotbugBuggedPlayer ~= nil ) then

        halo.Add( { HotbugBuggedPlayer }, Color( 255, 25, 25 ), 2, 2, 2, false, true )

    end

    if ( TouchTheBallHalos ) then

        halo.Add( ents.FindByClass( "sent_gw_ball" ) , Color( 0, 255, 10 ), 2, 2, 3, false, true )

    end

    if ( DrawGhostOverlay ) then

        DrawMaterialOverlay( "effects/invuln_overlay_blue", -5 )

    end

end

net.Receive( "ghost-hunter-draw-halos", function( len, ply )

    DrawHunterHalos = true
    
end )

net.Receive( "ghost-hunter-remove-halos", function( len, ply )

    DrawHunterHalos = false
    
end )

net.Receive( "ttt-draw-halos", function( len, ply )

    DrawTTTHalos = true
    
end )

net.Receive( "ttt-remove-halos", function( len, ply )

    DrawTTTHalos = false
    
end )

net.Receive( "hotbug-draw-all-halos", function( len, ply )

    HotbugDrawAllHalos = true
    HotbugBuggedPlayer = nil
    
end )

net.Receive( "hotbug-draw-bugged-halos", function( len, ply )

    HotbugDrawAllHalos = false
    HotbugBuggedPlayer = net.ReadEntity()
    
end )

net.Receive( "hotbug-remove-halos", function( len, ply )

    HotbugDrawAllHalos = false
    HotbugBuggedPlayer = nil
    
end )

net.Receive( "hide-and-seek-blind", function( len, ply )

    Blind = true
   
end )

net.Receive( "hide-and-seek-unblind", function( len, ply )

    Blind = false
    
end )

net.Receive( "touch-the-ball-draw-halos", function( len, ply )

    TouchTheBallHalos = true
   
end )

net.Receive( "touch-the-ball-remove-halos", function( len, ply )

    TouchTheBallHalos = false
    
end )

hook.Add( "HUDPaint", "cl-init-hud-paint", function()

    if (Blind) then

        surface.SetDrawColor( 255, 255, 255, 255 )
        surface.DrawRect( 0, 0, ScrW(), ScrH() )

    end
    
end )

net.Receive( "ghost-hunters-draw-overlay", function( len, ply )

    DrawGhostOverlay = true
    
end )

net.Receive( "ghost-hunters-remove-overlay", function( len, ply )

    DrawGhostOverlay = false
   
end )


function GM:ShouldCollide(ent1, ent2)

    if ((ent1:IsPlayer() && ent1:GetObserverMode() ~= OBS_MODE_NONE) || (ent2:IsPlayer() && ent2:GetObserverMode() ~= OBS_MODE_NONE)) then

        return false

    end

    return true

end