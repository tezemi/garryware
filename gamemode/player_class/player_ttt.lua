AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400
PLAYER.MaxHealth			= 100
PLAYER.StartHealth			= 100
PLAYER.StartArmor			= 0

function PLAYER:Loadout()

	self.Player:RemoveAllItems()

    self.Player:Give( "weapon_crowbar" )
    self.Player:Give( "weapon_pistol" )
    self.Player:Give( "weapon_smg1" )

    self.Player:GiveAmmo( 32, "Pistol", true )
    self.Player:GiveAmmo( 64, "SMG1", true )

end

player_manager.RegisterClass( "player_ttt", PLAYER, "player_default" )