AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400
PLAYER.MaxHealth			= 100
PLAYER.StartHealth			= 100
PLAYER.StartArmor			= 0

function PLAYER:Loadout()

	self.Player:RemoveAllItems()

    self.Player:Give( "weapon_physcannon" )

end

player_manager.RegisterClass( "player_dodgeball", PLAYER, "player_default" )