AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 300
PLAYER.RunSpeed				= 500
PLAYER.MaxHealth			= 100
PLAYER.StartHealth			= 100
PLAYER.StartArmor			= 100

function PLAYER:Loadout()

	self.Player:RemoveAllItems()

    self.Player:Give( "weapon_stunstick" )
    self.Player:Give( "weapon_357" )
    self.Player:Give( "weapon_ar2" )
    self.Player:Give( "weapon_crossbow" )
    self.Player:Give( "weapon_frag" )

    self.Player:GiveAmmo( 12, "357", true )
    self.Player:GiveAmmo( 64, "AR2", true )
    self.Player:GiveAmmo( 5, "XBowBolt", true )
    self.Player:GiveAmmo( 2, "Grenade", true )

end

player_manager.RegisterClass( "player_hunter", PLAYER, "player_default" )