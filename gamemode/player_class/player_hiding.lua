AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 200
PLAYER.MaxHealth			= 25
PLAYER.StartHealth			= 25
PLAYER.StartArmor			= 0

function PLAYER:Loadout()

	self.Player:RemoveAllItems()

end

player_manager.RegisterClass( "player_hiding", PLAYER, "player_default" )