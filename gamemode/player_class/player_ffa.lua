AddCSLuaFile()
DEFINE_BASECLASS( "player_default" )

local PLAYER = {}

PLAYER.WalkSpeed 			= 200
PLAYER.RunSpeed				= 400
PLAYER.MaxHealth			= 100
PLAYER.StartHealth			= 100
PLAYER.StartArmor			= 0

function PLAYER:Loadout()

	self.Player:RemoveAllItems()

    self.Player:Give( "weapon_crowbar" )
    self.Player:Give( "weapon_357" )
    self.Player:Give( "weapon_ar2" )
    self.Player:Give( "weapon_shotgun" )
    self.Player:Give( "weapon_frag" )

    self.Player:GiveAmmo( 12, "357", true )
    self.Player:GiveAmmo( 64, "AR2", true )
    self.Player:GiveAmmo( 12, "Buckshot", true )
    self.Player:GiveAmmo( 2, "Grenade", true )

end

player_manager.RegisterClass( "player_ffa", PLAYER, "player_default" )