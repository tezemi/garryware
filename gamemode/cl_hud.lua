local TimeLeft = 30
local CurrentGameName = "None"
local CurrentGameGoal = "Goal not set."
local CurrentMods = ""

net.Receive( "round-time", function( len, ply )

    TimeLeft = net.ReadInt( 32 )
    
end )

net.Receive( "send-goals", function( len, ply )

    CurrentGameName = net.ReadString()
    CurrentGameGoal = net.ReadString()
    CurrentMods = net.ReadString()
    
end )

function GM:HUDPaint()

    draw.DrawText( "Time Left: " .. TimeLeft, "Trebuchet24", 122, ScrH() - 115, Color( 255, 255, 0, 255 ), TEXT_ALIGN_CENTER )

    if ( CurrentGameName ~= "None" && CurrentGameName ~= "Base" ) then

        draw.DrawText( "Current Game: " .. CurrentGameName, "ChatFont", ScrW() / 2, ScrH() - 58, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
        draw.DrawText( "Goal: " .. CurrentGameGoal, "ChatFont", ScrW() / 2, ScrH() - 38, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )
        
    end

    if ( CurrentMods ~= "" ) then

        draw.DrawText( "Modifiers: " .. CurrentMods, "ChatFont", ScrW() / 2, ScrH() - 18, Color( 255, 255, 255, 255 ), TEXT_ALIGN_CENTER )

    end

end