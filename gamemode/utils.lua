
function CountdownInChat( whatWillHappen, amountOfTime )

    PrintMessage( 3, whatWillHappen .. amountOfTime .. " seconds!" )

    timer.Create("round-timer-message-0", amountOfTime / 2, 1, function() PrintMessage( 3, whatWillHappen .. amountOfTime / 2 .. " seconds!" ) end)
    timer.Create("round-timer-message-1", amountOfTime / 1.5, 1, function() PrintMessage( 3, whatWillHappen .. (amountOfTime - (amountOfTime / 1.5)) .. " seconds!" ) end)
    timer.Create("round-timer-message-2", amountOfTime - 3, 1, function() PrintMessage( 3, whatWillHappen .. "3 seconds!" ) end)
    timer.Create("round-timer-message-3", amountOfTime - 2, 1, function() PrintMessage( 3, whatWillHappen .. "2 seconds!" ) end)
    timer.Create("round-timer-message-4", amountOfTime - 1, 1, function() PrintMessage( 3, whatWillHappen .. "1 second!" ) end)

end

function EndCountdown()

    timer.Remove("round-timer-message-0")
    timer.Remove("round-timer-message-1")
    timer.Remove("round-timer-message-2")
    timer.Remove("round-timer-message-3")
    timer.Remove("round-timer-message-4")

end