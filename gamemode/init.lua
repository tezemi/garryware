DEFINE_BASECLASS( "gamemode_base" )

AddCSLuaFile( "cl_init.lua" )
AddCSLuaFile( "shared.lua" )
AddCSLuaFile( "games/base.lua" )
AddCSLuaFile( "cl_scoreboard.lua" )
AddCSLuaFile( "cl_hud.lua" )

include( "shared.lua" )
include( "utils.lua" )
include( "pointshop.lua" )

-- Modifiers
include( "modifiers/explosive_players.lua" )
include( "modifiers/low_gravity.lua" )
include( "modifiers/prop_rain.lua" )
include( "modifiers/heavy_gravity.lua" )
include( "modifiers/chaos_gravity.lua" )
include( "modifiers/explosive_props.lua" )
include( "modifiers/vampire_players.lua" )
include( "modifiers/zero_gravity.lua" )
include( "modifiers/sudden_death.lua" )


-- Games
include( "games/base.lua" )
include( "games/none.lua" )
include( "games/the_hunter.lua" )
include( "games/free_for_all.lua" )
include( "games/ghost_hunters.lua" )
include( "games/ttt.lua" )
include( "games/hotbug.lua" )
include( "games/hide_and_seek.lua" )
include( "games/touch_the_ball.lua" )
include( "games/dodgeball.lua" )


-- Variables
CURRENT_GAME = NONE

local GAME_MODES = 
{
    FREE_FOR_ALL,
    THE_HUNTER,
    GHOST_HUNTER,
    TTT,
    HOTBUG,
    HIDE_AND_SEEK,
    TOUCH_THE_BALL,
    DODGEBALL
}

-- Net
util.AddNetworkString( "round-time" )

-- Functions
function ChangeGame( game )

    CURRENT_GAME = game
    
    if (CURRENT_GAME ~= NONE) then

        PrintMessage( 3, "A new game has started! The game is " .. CURRENT_GAME.Name .. "!" )

    end    

    CURRENT_GAME:Start()

end

function EndGame()
    
    if CURRENT_GAME ~= NONE then

        ChangeGame( NONE )

        local playedGameCount = 0
        for key, game in pairs(GAME_MODES) do

            if (game.HasBeenPlayed) then

                playedGameCount = playedGameCount + 1

            end

        end

        if (playedGameCount >= #GAME_MODES) then

            for key, val in pairs(GAME_MODES) do

                val.HasBeenPlayed = false

            end

            PrintMessage( 3, "It's time for a cleanup! The map has been cleaned!" )
            game.CleanUpMap()   

        end      

    else

        local debugCount = 0
        local selectedGame = GAME_MODES[ math.random( #GAME_MODES ) ]
        while (selectedGame.HasBeenPlayed && debugCount < 100) do
            
            debugCount = debugCount + 1
            selectedGame = GAME_MODES[ math.random( #GAME_MODES ) ]            

        end

        selectedGame.HasBeenPlayed = true
        ChangeGame( selectedGame )

    end

end


-- Hooks
function GM:Initialize() 

    ChangeGame( NONE )
    timer.Create( "send-time-to-players", 0.5, 0, function() 

        net.Start( "round-time" )
        if ( timer.Exists( "round-timer" ) ) then

            net.WriteInt( timer.TimeLeft( "round-timer" ), 32 )

        else

            net.WriteInt( 0, 32 )

        end

        net.Broadcast()

    end )

end

function GM:PlayerInitialSpawn( ply, transition ) 

    ply:SetNWInt( "GamesWon", 0 )
    ply:SetNWInt( "Points", 0 )
    
end

function GM:PlayerShouldTakeDamage( ply, attacker ) 
    
    if (!CURRENT_GAME.DamageEnabled) then
        
        return false
    
    end

    if (ply:IsPlayer() && attacker:IsPlayer() && ply ~= attacker) then

        if (!CURRENT_GAME.AllowFriendlyFire && ply:Team() == attacker:Team()) then

            return false

        end

    end

    return true

end

function GM:PlayerSpawn( ply )

    if (CURRENT_GAME.AllowSpawning) then

        player_manager.SetPlayerClass( ply, CURRENT_GAME.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )        

    elseif (!CURRENT_GAME.InitSpawns) then

        ply:Spectate( OBS_MODE_ROAMING )
        ply:SetObserverMode( OBS_MODE_ROAMING )        

    end

    ply:SetTeam(CURRENT_GAME.DefaultTeam)

end

function GM:PlayerCanPickupItem( ply, item ) 

    if (CURRENT_GAME.AllowItemPickup) then

        return true

    else

        return false

    end

end

function GM:PlayerCanPickupWeapon( ply, wep ) 

    if (CURRENT_GAME.AllowItemPickup || wep:GetClass() == "weapon_noisemaker" || wep:GetClass() == "weapon_hotbug" || ((CURRENT_GAME == HIDE_AND_SEEK || CURRENT_GAME == TOUCH_THE_BALL) && wep:GetClass() == "weapon_crowbar")) then

        return true

    else

        local allowed = false
        for k, v in pairs(CURRENT_GAME.AllowedWeapons) do

            if (wep:GetClass() == v) then

                allowed = true

            end

        end

        return allowed

    end
    
end

function GM:PlayerDeathSound()

    return false

end

function GM:PlayerUse( ply, ent ) 

end

function GM:CanPlayerSuicide( ply )

    if ( ply:GetObserverMode() ~= OBS_MODE_NONE ) then
        
        return false
        
    else
        
        return true
        
    end
    
end

function GM:AllowPlayerPickup( ply, ent )
	
    if ( ply:GetObserverMode() ~= OBS_MODE_NONE ) then
        
        return false
        
    else
        
        return true
        
    end	
    
end

function GM:PlayerSwitchFlashlight( ply, enabled ) 

    if ( ply:GetObserverMode() ~= OBS_MODE_NONE ) then
        
        return false
        
    else
        
        return true
        
    end	

end

function GM:ShouldCollide( ent1, ent2 )

    if ((ent1:IsPlayer() && ent1:GetObserverMode() ~= OBS_MODE_NONE && ent2:IsPlayer()) || (ent2:IsPlayer() && ent2:GetObserverMode() ~= OBS_MODE_NONE && ent1:IsPlayer())) then

        return false

    end

    return true

end

function GM:GetFallDamage( ply, speed )

    return ( speed / 8 )
    
end

function GM:PlayerUse( ply, ent )

    if ( ply:GetObserverMode() ~= OBS_MODE_NONE ) then
        
        return false
        
    else
        
        return true
        
    end	

end

function GM:KeyPress( ply, key )
    
    function RotateSpectate( amount )
        
        ply:SetNWInt( "PlayerFollowing", ply:GetNWInt( "PlayerFollowing", 0 ) + amount )

        local players = player.GetAll()
        local spectate = players[ ply:GetNWInt( "PlayerFollowing", 0 ) ]
        local debugCount = 0
        
        while ( (spectate == nil || !spectate:Alive() || spectate:GetObserverMode() ~= OBS_MODE_NONE) && debugCount < 100  ) do

            if ( ply:GetNWInt( "PlayerFollowing", 0 ) >= #players ) then

                ply:SetNWInt( "PlayerFollowing", 0 )

            elseif ( ply:GetNWInt( "PlayerFollowing", 0 ) < 0 ) then

                ply:SetNWInt( "PlayerFollowing", #players )

            else

                ply:SetNWInt( "PlayerFollowing", ply:GetNWInt( "PlayerFollowing", 0 ) + amount )

            end

            debugCount = debugCount + 1
            
            spectate = players[ ply:GetNWInt( "PlayerFollowing", 0 ) ]

        end

        if ( ply:GetObserverMode() ~= OBS_MODE_CHASE && ply:GetObserverMode() ~= OBS_MODE_IN_EYE ) then

            ply:Spectate( OBS_MODE_CHASE )
            ply:SetObserverMode( OBS_MODE_CHASE ) 
        
        end

        ply:SpectateEntity( spectate ) 

    end

    if ( ply:GetObserverMode() == OBS_MODE_NONE ) then return end

    if ( key == IN_ATTACK ) then
        
        RotateSpectate( 1 )

    elseif ( key == IN_ATTACK2 ) then
        
        RotateSpectate( -1 )

    elseif ( key == IN_RELOAD && ply:GetObserverTarget() ~= nil ) then

        if ( ply:GetObserverMode() == OBS_MODE_CHASE ) then

            ply:Spectate( OBS_MODE_IN_EYE )
            ply:SetObserverMode( OBS_MODE_IN_EYE )

        else

            ply:Spectate( OBS_MODE_CHASE )
            ply:SetObserverMode( OBS_MODE_CHASE )

        end

    elseif ( key == IN_DUCK ) then

        ply:Spectate( OBS_MODE_ROAMING )
        ply:SetObserverMode( OBS_MODE_ROAMING )

    end

end
