
include( "player_class/player_idle.lua" )
include( "player_class/player_unarmed.lua" )
include( "player_class/player_hunted.lua" )
include( "player_class/player_hunter.lua" )
include( "player_class/player_ffa.lua" )
include( "player_class/player_ghost.lua" )
include( "player_class/player_ttt.lua" )
include( "player_class/player_hiding.lua" )
include( "player_class/player_dodgeball.lua" )

GM.Name = "Garryware"
GM.Author = "Tezemi"
GM.Email = "N/A"
GM.Website = "N/A"

team.SetUp(0, "No Team", Color(255, 255, 255));
team.SetUp(1, "Hunter", Color(255, 0, 0));
team.SetUp(2, "Hunted", Color(0, 255, 0));
team.SetUp(3, "Red Team", Color(255, 0, 0));
team.SetUp(4, "Blue Team", Color(0, 0, 255));
team.SetUp(5, "Ghost", Color(200, 200, 200, 0));
team.SetUp(6, "Ghost Hunters", Color(255, 255, 0));

function GM:Initialize()
	
end
