
BASE = {}

BASE.Name = "Base"
BASE.Goal = "Goal not set."
BASE.GoalAlt = nil
BASE.DefaultClass = "player_idle"
BASE.AllowSpawning = true
BASE.DamageEnabled = false
BASE.AllowItemPickup = true
BASE.DefaultTeam = 0
BASE.AllowFriendlyFire = false
BASE.RoundTime = 120
BASE.HasBeenPlayed = false
BASE.InitSpawns = false
BASE.VictoryPoints = 200
BASE.KillPoints = 25
BASE.AllowedWeapons = { }
BASE.EnabledModifiers = { }
BASE.AvailableModifiers = 
{ 
    EXPLOSIVE_PLAYERS, 
    LOW_GRAVITY, 
    PROP_RAIN, 
    HEAVY_GRAVITY, 
    CHAOS_GRAVITY,
    VAMPIRE_PLAYERS,
    EXPLOSIVE_PROPS,
    ZERO_GRAVITY,
    SUDDEN_DEATH
}

BASE.DebugMod = nil

util.AddNetworkString( "send-goals" )

function BASE:New ( o )

    o = o or {}
    setmetatable(o, self)
    self.__index = self

    return o

end

function BASE:Start()

    timer.Create( "round-timer", CURRENT_GAME.RoundTime, 1, function() CURRENT_GAME:OnTimeUp() end )

    BASE.EnabledModifiers = { }
    if (BASE.DebugMod ~= nil) then

        BASE.DebugMod:Enable()

    else
        
        for key, mod in pairs(CURRENT_GAME.AvailableModifiers) do

            if (!mod.Enabled && math.random() < 0.05--[[#CURRENT_GAME.AvailableModifiers / 13]]) then

                mod:Enable()
                table.insert( BASE.EnabledModifiers, mod )

            end        

        end

    end

    BASE.InitSpawns = true
    for key, ply in pairs( player.GetAll() ) do

        if (!ply:Alive()) then

            ply:Spectate( OBS_MODE_NONE )
            ply:SetObserverMode( OBS_MODE_NONE )
            ply:Spawn()

        end

    end

    BASE.InitSpawns = false

    CURRENT_GAME:SendGoals( player.GetAll(), CURRENT_GAME.Goal )

end

function BASE:OnTimeUp()

end

function BASE:SendGoals( goalPlayers, goal )

    for k, v in pairs( goalPlayers ) do

        net.Start( "send-goals" )
        net.WriteString( CURRENT_GAME.Name )
        net.WriteString( goal )

        local mods = ""
        for k, v in pairs( CURRENT_GAME.EnabledModifiers ) do

            mods = mods .. v.Name
            if ( k < #CURRENT_GAME.EnabledModifiers ) then

                mods = mods .. ", "

            end

        end

        net.WriteString( mods )
        net.Send( v )

    end

end

function BASE:PlayerVictory( players )

    if ( #players > 1 ) then

        local message = ""
        for k, v in pairs(players) do

            message = message .. v:Nick() .. ", "
            v:SetNWInt( "Points", v:GetNWInt( "Points", 0 ) + math.floor( CURRENT_GAME.VictoryPoints / #players ) )
            v:SetNWInt( "GamesWon", v:GetNWInt( "GamesWon", 0 ) + 1 )

        end

        message = message .. " are the winners!"
        PrintMessage( 3, message )

    else

        local ply = players[1]

        if ( ply == nil ) then

            PrintMessage( 3, "The game is over!" )

        else

            PrintMessage( 3, ply:Nick() .. " wins!" )
            ply:SetNWInt( "Points", ply:GetNWInt( "Points", 0 ) + CURRENT_GAME.VictoryPoints )
            ply:SetNWInt( "GamesWon", ply:GetNWInt( "GamesWon", 0 ) + 1 )

        end

    end

end

function BASE:PlayerDefeat( ply )

    local winningPlayers = {}
    for k, v in pairs( player.GetAll() ) do

        if ( v ~= ply ) then

            table.insert( winningPlayers, v )

        end

    end

    CURRENT_GAME:PlayerVictory( winningPlayers )

end

function BASE:CleanupAndEnd()

    for key, mod in pairs(CURRENT_GAME.AvailableModifiers) do

        if (mod.Enabled) then

            mod:Disable()

        end        

    end

    timer.Remove("round-timer")

    EndCountdown()
    EndGame()

end