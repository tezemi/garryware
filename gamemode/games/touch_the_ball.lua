DEFINE_BASECLASS( "gamemode_base" )

TOUCH_THE_BALL = BASE:New()

TOUCH_THE_BALL.Name = "Touch the Ball"
TOUCH_THE_BALL.Goal = "Be the last person to touch the ball!"
TOUCH_THE_BALL.DefaultClass = "player_hunted"
TOUCH_THE_BALL.AllowSpawning = false
TOUCH_THE_BALL.DamageEnabled = false
TOUCH_THE_BALL.AllowFriendlyFire = true
TOUCH_THE_BALL.AllowItemPickup = false
TOUCH_THE_BALL.RoundTime = 80
TOUCH_THE_BALL.AvailableModifiers = { LOW_GRAVITY, CHAOS_GRAVITY, HEAVY_GRAVITY, ZERO_GRAVITY }

TOUCH_THE_BALL.BallTouchingPlayer = nil
TOUCH_THE_BALL.Ball = nil

util.AddNetworkString( "touch-the-ball-draw-halos" )
util.AddNetworkString( "touch-the-ball-remove-halos" )

function TOUCH_THE_BALL:Start()

    BASE:Start()

    PrintMessage( 3, "Be the last person to touch the ball!" )

    for key, ply in pairs( player.GetAll() ) do

        ply:SetTeam(TOUCH_THE_BALL.DefaultTeam)

        player_manager.SetPlayerClass( ply, TOUCH_THE_BALL.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )

    end

    TOUCH_THE_BALL.BallTouchingPlayer = nil
    TOUCH_THE_BALL:SpawnBall()

    net.Start( "touch-the-ball-draw-halos" )
    net.Broadcast()

end

function TOUCH_THE_BALL:OnTimeUp()

    if (TOUCH_THE_BALL.BallTouchingPlayer ~= nil) then

        CURRENT_GAME:PlayerVictory( { TOUCH_THE_BALL.BallTouchingPlayer } )
    
    else

        PrintMessage( 3, "Nobody touched the ball... Nobody wins..." )

    end

    TOUCH_THE_BALL:CleanupAndEnd()

end

function TOUCH_THE_BALL:CleanupAndEnd()

    net.Start( "touch-the-ball-remove-halos" )
    net.Broadcast()

    if (TOUCH_THE_BALL.Ball ~= nil) then

        TOUCH_THE_BALL.Ball:Remove()

    end

    BASE:CleanupAndEnd()

end

function TOUCH_THE_BALL:SpawnBall()

    local allPlayers = player.GetAll()
    local playerToSpawnOn = allPlayers[ math.random( #allPlayers ) ]
    local ent = ents.Create( "sent_gw_ball" )
    
    ent:SetPos( playerToSpawnOn:GetPos() + Vector( 0, 0, 50 ) )
    ent:Spawn()

    TOUCH_THE_BALL.Ball = ent

end

hook.Add("EntityTakeDamage", "TOUCH_THE_BALL:EntityTakeDamage", function( target, dmg )

    if (CURRENT_GAME ~= TOUCH_THE_BALL) then return end

    if (target:GetClass() == "sent_gw_ball" && dmg:GetAttacker():IsPlayer()) then

        TOUCH_THE_BALL.BallTouchingPlayer = dmg:GetAttacker()
        TOUCH_THE_BALL.BallTouchingPlayer:ChatPrint( "You touched the ball!" )
        for key, ply in pairs( player.GetAll() ) do

            if (ply ~= TOUCH_THE_BALL.BallTouchingPlayer) then
                
                ply:ChatPrint( TOUCH_THE_BALL.BallTouchingPlayer:Nick() .. " touched the ball! You need to touch it!" )
    
            end
    
        end

    end
    
end)

hook.Add("Think", "TOUCH_THE_BALL:Think", function()

    if (CURRENT_GAME ~= TOUCH_THE_BALL) then return end

    if (TOUCH_THE_BALL.Ball == nil) then

        TOUCH_THE_BALL:SpawnBall()

    end

end)
