DEFINE_BASECLASS( "gamemode_base" )

HIDE_AND_SEEK = BASE:New()

HIDE_AND_SEEK.Name = "Hide and Seek"
HIDE_AND_SEEK.Goal = "Hide from the seeker!"
HIDE_AND_SEEK.GoalAlt = "Find and kill all of the other players!"
HIDE_AND_SEEK.DefaultClass = "player_hiding"
HIDE_AND_SEEK.AllowSpawning = false
HIDE_AND_SEEK.DamageEnabled = true
HIDE_AND_SEEK.AllowItemPickup = false
HIDE_AND_SEEK.DefaultTeam = 2
HIDE_AND_SEEK.RoundTime = 240
HIDE_AND_SEEK.AvailableModifiers = { LOW_GRAVITY, CHAOS_GRAVITY, HEAVY_GRAVITY, ZERO_GRAVITY }

HIDE_AND_SEEK.Seeker = nil

util.AddNetworkString( "hide-and-seek-blind" )
util.AddNetworkString( "hide-and-seek-unblind" )

function HIDE_AND_SEEK:Start()

    BASE:Start()

    local allPlayers = player.GetAll()
    HIDE_AND_SEEK.Seeker = allPlayers[ math.random( #allPlayers ) ]

    HIDE_AND_SEEK.Seeker:ChatPrint( "You are the Seeker! Kill all of the other players! You will be unlocked in one minute..." )

    HIDE_AND_SEEK.Seeker:SetTeam(1)

    HIDE_AND_SEEK.Seeker:Lock()

    net.Start( "hide-and-seek-blind" )
    net.Send( HIDE_AND_SEEK.Seeker )

    timer.Create("hide-and-seek-release-timer", 60, 1, function()

         HIDE_AND_SEEK.Seeker:UnLock() 

         net.Start( "hide-and-seek-unblind" )
         net.Send( HIDE_AND_SEEK.Seeker )

    end)

    player_manager.SetPlayerClass( HIDE_AND_SEEK.Seeker, "player_hunted" )
    BaseClass.PlayerSpawn( self, HIDE_AND_SEEK.Seeker )

    for key, ply in pairs( player.GetAll() ) do

        if (ply ~= HIDE_AND_SEEK.Seeker) then
            
            ply:ChatPrint( "It's hide and seek! Go and hide! " .. HIDE_AND_SEEK.Seeker:Nick() .. " is the Seeker! You have one minute to hide!" )

            ply:SetTeam(HIDE_AND_SEEK.DefaultTeam)

            player_manager.SetPlayerClass( ply, HIDE_AND_SEEK.DefaultClass )
            BaseClass.PlayerSpawn( self, ply )

        end

    end

    CURRENT_GAME:SendGoals( { HIDE_AND_SEEK.Seeker }, CURRENT_GAME.GoalAlt )

end

function HIDE_AND_SEEK:OnTimeUp()

    CURRENT_GAME:PlayerDefeat( HIDE_AND_SEEK.Seeker )

    HIDE_AND_SEEK:CleanupAndEnd()

end

function HIDE_AND_SEEK:CleanupAndEnd()

    timer.Remove("hide-and-seek-release-timer")

    BASE:CleanupAndEnd()
    
end

local function CheckPlayerCount( updatePly )

    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1

        end

    end

    if (alivePlayers <= 1) then

        CURRENT_GAME:PlayerVictory( { HIDE_AND_SEEK.Seeker } )

        HIDE_AND_SEEK:CleanupAndEnd()
    
    else

        PrintMessage( 3, "There are " .. alivePlayers - 1 .. " of the hiding left!" )

    end

end

hook.Add("PlayerDeath", "HIDE_AND_SEEK:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= HIDE_AND_SEEK) then return end

    if ( victim == HIDE_AND_SEEK.Seeker ) then
       
        CURRENT_GAME:PlayerDefeat( { HIDE_AND_SEEK.Seeker } )

        HIDE_AND_SEEK:CleanupAndEnd()

    else

        CheckPlayerCount( victim )
                
        if (attacker:IsPlayer() && victim:IsPlayer() && victim ~= attacker) then

            attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

        end

    end
    
end)

hook.Add("PlayerDisconnected", "HIDE_AND_SEEK:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= HIDE_AND_SEEK) then return end

    if ( ply == HIDE_AND_SEEK.Seeker ) then
       
        PrintMessage( 3, "The Seeker just disconnected..." )

        CURRENT_GAME:PlayerDefeat( HIDE_AND_SEEK.Seeker )

        HIDE_AND_SEEK:CleanupAndEnd()

    else

        CheckPlayerCount( ply )

    end
    
end)