DEFINE_BASECLASS( "gamemode_base" )

TTT = BASE:New()

TTT.Name = "Impromptu  TTT"
TTT.Goal = "There is a traitor amoungst you. Find and kill the traitor, or survive until the time is up."
TTT.GoalAlt = "You are a traitor! Kill all of the other players! Don't get caught!"
TTT.DefaultClass = "player_ttt"
TTT.AllowSpawning = false
TTT.DamageEnabled = true
TTT.DefaultTeam = 0
TTT.AllowFriendlyFire = true
TTT.RoundTime = 300

TTT.Traitor = nil

util.AddNetworkString( "ttt-draw-halos" )
util.AddNetworkString( "ttt-remove-halos" )

function TTT:Start()

    BASE:Start()
    
    local allPlayers = player.GetAll()
    TTT.Traitor = allPlayers[ math.random( #allPlayers ) ]

    TTT.Traitor:ChatPrint( "You are a traitor! Nobody else knows. Kill all of the other players..." )

    for key, ply in pairs( player.GetAll() ) do

        if (ply ~= TTT.Traitor) then
            
            ply:ChatPrint( "There's a traitor! Survive until the end of the round, or kill the traitor!" )

        end

        ply:SetTeam(TTT.DefaultTeam)

        player_manager.SetPlayerClass( ply, TTT.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )

    end

    net.Start( "ttt-draw-halos" )
    net.Send( TTT.Traitor )

    CURRENT_GAME:SendGoals( { TTT.Traitor }, CURRENT_GAME.GoalAlt )

end

function TTT:OnTimeUp()

    CURRENT_GAME:PlayerDefeat( TTT.Traitor )

    TTT:CleanupAndEnd()

end

function TTT:CleanupAndEnd()

    BASE:CleanupAndEnd()
    
    net.Start( "ttt-remove-halos" )
    net.Send( TTT.Traitor )

end

local function CheckPlayerCount( updatePly )

    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1

        end

    end

    if (alivePlayers <= 1) then

        CURRENT_GAME:PlayerVictory( { TTT.Traitor } )

        TTT:CleanupAndEnd()
    
    else

        TTT.Traitor:ChatPrint( "There are " .. alivePlayers - 1 .. " of the innocents left!" )

    end

end

hook.Add("PlayerDeath", "TTT:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= TTT) then return end

    if ( victim == TTT.Traitor ) then

        CURRENT_GAME:PlayerDefeat( TTT.Traitor )        

        TTT:CleanupAndEnd()

    else

        if ( victim ~= TTT.Traitor && attacker ~= TTT.Traitor && victim ~= attacker && attacker:IsPlayer() ) then

            attacker:ChatPrint( victim:Nick() .. " was not the traitor..." )
            attacker:SetHealth( attacker:Health() / 2 )
            attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) - CURRENT_GAME.KillPoints )
        
        elseif ( attacker == TTT.Traitor && victim ~= TTT.Traitor ) then

            attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

        end

        CheckPlayerCount( victim )

    end
    
end)

hook.Add("PlayerDisconnected", "TTT:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= TTT) then return end

    if ( ply == TTT.Traitor ) then
       
        PrintMessage( 3, "The traitor just disconnected..." )

        CURRENT_GAME:PlayerDefeat( TTT.Traitor )

        TTT:CleanupAndEnd()

    else

        CheckPlayerCount( ply )

    end
    
end)