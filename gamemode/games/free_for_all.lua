DEFINE_BASECLASS( "gamemode_base" )

FREE_FOR_ALL = BASE:New()

FREE_FOR_ALL.Name = "Free for All"
FREE_FOR_ALL.Goal = "Kill all of the other players!"
FREE_FOR_ALL.DefaultClass = "player_ffa"
FREE_FOR_ALL.AllowSpawning = false
FREE_FOR_ALL.DamageEnabled = true
FREE_FOR_ALL.AllowFriendlyFire = true

FREE_FOR_ALL.CullHealthLoss = 3

function FREE_FOR_ALL:Start()

    BASE:Start()

    for key, ply in pairs( player.GetAll() ) do

        ply:SetTeam(FREE_FOR_ALL.DefaultTeam)

        player_manager.SetPlayerClass( ply, FREE_FOR_ALL.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )

    end

end

function FREE_FOR_ALL:OnTimeUp()

    timer.Create("cull-players", 1, 0, function ()

        for key, ply in pairs( player.GetAll() ) do

            if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE) then
                
                ply:SetHealth(ply:Health() - FREE_FOR_ALL.CullHealthLoss)

                if (ply:Health() <= 0) then

                    ply:Kill()

                end
    
            end
    
        end        

    end)

end

function FREE_FOR_ALL:CleanupAndEnd()

    timer.Remove("cull-players")

    BASE:CleanupAndEnd()

end

local function CheckPlayerCount( updatePly )

    local lastAlivePlayer = nil
    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1
            lastAlivePlayer = ply

        end

    end

    if (alivePlayers <= 1) then

        if (lastAlivePlayer == nil) then

            PrintMessage( 3, "The free for all is over!" )

        else

            CURRENT_GAME:PlayerVictory( { lastAlivePlayer } )

        end
        
        FREE_FOR_ALL:CleanupAndEnd()
    
    else

        PrintMessage( 3, "There are " .. alivePlayers .. " left!" )

    end

end

hook.Add("PlayerDeath", "FREE_FOR_ALL:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= FREE_FOR_ALL) then return end

    CheckPlayerCount( victim )

    if (attacker:IsPlayer() && victim:IsPlayer() && victim ~= attacker) then

        attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

    end
    
end)

hook.Add("PlayerDisconnected", "FREE_FOR_ALL:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= FREE_FOR_ALL) then return end

    CheckPlayerCount( ply )
    
end)