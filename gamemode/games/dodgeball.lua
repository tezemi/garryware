DEFINE_BASECLASS( "gamemode_base" )

DODGEBALL = BASE:New()

DODGEBALL.Name = "Dodgeball"
DODGEBALL.Goal = "Kill all of the other players!"
DODGEBALL.DefaultClass = "player_dodgeball"
DODGEBALL.AllowSpawning = false
DODGEBALL.DamageEnabled = true
DODGEBALL.AllowFriendlyFire = true
DODGEBALL.AllowItemPickup = false
DODGEBALL.RoundTime = 120
DODGEBALL.AllowedWeapons = { "weapon_physcannon" }
DODGEBALL.AvailableModifiers = { EXPLOSIVE_PLAYERS, EXPLOSIVE_PROPS, PROP_RAIN, SUDDEN_DEATH }

DODGEBALL.CullHealthLoss = 3

function DODGEBALL:Start()

    BASE:Start()

    for key, ply in pairs( player.GetAll() ) do

        ply:SetTeam(DODGEBALL.DefaultTeam)

        player_manager.SetPlayerClass( ply, DODGEBALL.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )

        local ent = ents.Create( "prop_physics" )
        ent:SetModel( "models/xqm/rails/gumball_1.mdl" )
        ent:SetPos( ply:GetPos() + Vector( math.random() * 100, math.random() * 100, 50 ) )
        ent:SetColor( Color( math.random() * 255, math.random() * 255, math.random() * 255, 255 ) )
        ent:Spawn()

    end

end

function DODGEBALL:OnTimeUp()

    timer.Create("cull-players", 1, 0, function ()

        for key, ply in pairs( player.GetAll() ) do

            if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE) then
                
                ply:SetHealth(ply:Health() - DODGEBALL.CullHealthLoss)

                if (ply:Health() <= 0) then

                    ply:Kill()

                end
    
            end
    
        end        

    end)

end

function DODGEBALL:CleanupAndEnd()

    timer.Remove("cull-players")

    BASE:CleanupAndEnd()

end

local function CheckPlayerCount( updatePly )

    local lastAlivePlayer = nil
    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1
            lastAlivePlayer = ply

        end

    end

    if (alivePlayers <= 1) then

        if (lastAlivePlayer == nil) then

            PrintMessage( 3, "The game is over!" )

        else

            CURRENT_GAME:PlayerVictory( { lastAlivePlayer } )

        end
        
        FREE_FOR_ALL:CleanupAndEnd()
    
    else

        PrintMessage( 3, "There are " .. alivePlayers .. " left!" )

    end

end

hook.Add("PlayerDeath", "DODGEBALL:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= DODGEBALL) then return end

    CheckPlayerCount( victim )

    if (attacker:IsPlayer() && victim:IsPlayer() && victim ~= attacker) then

        attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

    end
    
end)

hook.Add("PlayerDisconnected", "DODGEBALL:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= DODGEBALL) then return end

    CheckPlayerCount( ply )
    
end)
