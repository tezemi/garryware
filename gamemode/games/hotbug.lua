DEFINE_BASECLASS( "gamemode_base" )

HOTBUG = BASE:New()

HOTBUG.Name = "Hotbug"
HOTBUG.Goal = "Pass the hotbug to another player before it explodes!"
HOTBUG.DefaultClass = "player_idle"
HOTBUG.AllowSpawning = false
HOTBUG.DamageEnabled = true
HOTBUG.AllowFriendlyFire = true
HOTBUG.AllowItemPickup = false
HOTBUG.RoundTime = 10
HOTBUG.AvailableModifiers = 
{    
    LOW_GRAVITY, 
    HEAVY_GRAVITY, 
    CHAOS_GRAVITY,
    ZERO_GRAVITY
}

HOTBUG.BuggedPlayer = nil
HOTBUG.CleaningAndEnding = false

util.AddNetworkString( "hotbug-draw-all-halos" )
util.AddNetworkString( "hotbug-draw-bugged-halos" )
util.AddNetworkString( "hotbug-remove-halos" )

local function CheckPlayerCount( showupdate )

    local lastAlivePlayer = nil
    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE) then

            alivePlayers = alivePlayers + 1
            lastAlivePlayer = ply

        end

    end

    if (alivePlayers <= 1) then

        HOTBUG.CleaningAndEnding = true

        if (lastAlivePlayer == nil) then

            PrintMessage( 3, "The game is over!" )

        else

            CURRENT_GAME:PlayerVictory( { lastAlivePlayer } )

        end
        
        HOTBUG:CleanupAndEnd()
    
    elseif (showupdate) then

        PrintMessage( 3, "There are " .. (alivePlayers - 1) .. " left!" )

    end 

end

function HOTBUG:Start()

    BASE:Start()

    HOTBUG.RoundTime = 10
    HOTBUG.CleaningAndEnding = false

    for key, ply in pairs( player.GetAll() ) do

        ply:SetTeam(HOTBUG.DefaultTeam)

        player_manager.SetPlayerClass( ply, HOTBUG.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )

    end

    HOTBUG:GetNewBuggedPlayer()

end

function HOTBUG:OnTimeUp()

    if (HOTBUG.CleaningAndEnding) then return end

    if (HOTBUG.BuggedPlayer ~= nil && HOTBUG.BuggedPlayer:Alive() && HOTBUG.BuggedPlayer:GetObserverMode() == OBS_MODE_NONE) then

        HOTBUG.BuggedPlayer:Kill()
        
        local radius = 300
        local explode = ents.Create( "env_explosion" )
        explode:SetPos( HOTBUG.BuggedPlayer:GetPos() + Vector( 0, 2, 0 ) )
        explode:SetOwner( HOTBUG.BuggedPlayer )
        explode:SetKeyValue( "iMagnitude", "120" )
        explode:Fire( "Explode", 0, 0 )
        explode:EmitSound( "weapon_AWP.Single", 400, 100, 0.75 )

        CheckPlayerCount(false)

        if (HOTBUG.CleaningAndEnding) then return end

        HOTBUG.RoundTime = HOTBUG.RoundTime + 2.5

    end

    HOTBUG:GetNewBuggedPlayer()

    timer.Create("round-timer", CURRENT_GAME.RoundTime, 1, function() CURRENT_GAME:OnTimeUp() end)

end

function HOTBUG:CleanupAndEnd()

    HOTBUG.CleaningAndEnding = true

    net.Start( "hotbug-remove-halos" )
    net.Broadcast()

    for key, val in pairs( ents.FindByClass( "sent_hotbug" ) ) do

        val:Remove()

    end

    BASE:CleanupAndEnd()

end

function HOTBUG:GetNewBuggedPlayer()

    local debugCount = 0
    local allPlayers = player.GetAll()
    HOTBUG.BuggedPlayer = allPlayers[ math.random( #allPlayers ) ]
    while (!HOTBUG.BuggedPlayer:Alive() || HOTBUG.BuggedPlayer:GetObserverMode() ~= OBS_MODE_NONE) do

        HOTBUG.BuggedPlayer = allPlayers[ math.random( #allPlayers ) ]
        debugCount = debugCount + 1
        if ( debugCount ) then

            break

        end

    end

    HOTBUG.BuggedPlayer:Give("weapon_hotbug")

    for key, ply in pairs( player.GetAll() ) do

        if (HOTBUG.BuggedPlayer == ply) then

            net.Start( "hotbug-draw-all-halos" )
            net.Send( ply )

            ply:ChatPrint( "Give the hotbug to another player before it explodes!" )

        else

            net.Start( "hotbug-draw-bugged-halos" )
            net.WriteEntity( HOTBUG.BuggedPlayer )
            net.Send( ply )

            ply:ChatPrint( HOTBUG.BuggedPlayer:Nick() .. " has the hotbug!" )

        end

    end

end

hook.Add("PlayerSwitchWeapon", "HOTBUG:PlayerSwitchWeapon", function( ply, oldWeapon, newWeapon )

    if (CURRENT_GAME ~= HOTBUG) then return end

    if ( newWeapon:GetClass() == "weapon_hotbug" ) then

        HOTBUG.BuggedPlayer = ply

    end
    
end)

hook.Add("PlayerDeath", "HOTBUG:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= HOTBUG) then return end

    CheckPlayerCount( true )
    
end)

hook.Add("PlayerDisconnected", "HOTBUG:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= HOTBUG) then return end

    CheckPlayerCount( true )
    
end)