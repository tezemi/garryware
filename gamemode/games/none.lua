DEFINE_BASECLASS( "gamemode_base" )

NONE = BASE:New()

NONE.Name = "None"
NONE.RoundTime = 30
NONE.AllowItemPickup = false
NONE.AvailableModifiers = { }

function NONE:Start()

    BASE:Start(self)

    for key, ply in pairs(player.GetAll()) do
        
        local needsRespawn = false
        if (ply:GetObserverMode() ~= OBS_MODE_NONE) then

            ply:KillSilent()

            ply:Spectate( OBS_MODE_NONE )
            ply:SetObserverMode( OBS_MODE_NONE )

            needsRespawn = true

        end

        ply:SetTeam(NONE.DefaultTeam)

        player_manager.SetPlayerClass( ply, NONE.DefaultClass )
        BaseClass.PlayerSpawn( self, ply )
        
        if ( needsRespawn ) then

            ply:Spawn()
        
        end

    end

end

function NONE:OnTimeUp()

    BASE:OnTimeUp()

    BASE:CleanupAndEnd()

end
