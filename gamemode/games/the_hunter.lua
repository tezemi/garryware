DEFINE_BASECLASS( "gamemode_base" )

THE_HUNTER = BASE:New()

THE_HUNTER.Name = "The Hunter"
THE_HUNTER.Goal = "You are being hunted! Hide from the hunter, or try to defend yourself!"
THE_HUNTER.GoalAlt = "You are the hunter! Kill all of the other players!"
THE_HUNTER.DefaultClass = "player_hunted"
THE_HUNTER.AllowSpawning = false
THE_HUNTER.DamageEnabled = true
THE_HUNTER.DefaultTeam = 2

THE_HUNTER.HunterPlayer = nil

util.AddNetworkString( "ghost-hunter-draw-halos" )
util.AddNetworkString( "ghost-hunter-remove-halos" )

function THE_HUNTER:Start()

    BASE:Start()

    local allPlayers = player.GetAll()
    THE_HUNTER.HunterPlayer = allPlayers[ math.random( #allPlayers ) ]

    THE_HUNTER.HunterPlayer:ChatPrint( "You are the Hunter! Kill all of the other players!" )

    THE_HUNTER.HunterPlayer:SetTeam(1)

    player_manager.SetPlayerClass( THE_HUNTER.HunterPlayer, "player_hunter" )
    BaseClass.PlayerSpawn( self, THE_HUNTER.HunterPlayer )

    for key, ply in pairs( player.GetAll() ) do

        if (ply ~= THE_HUNTER.HunterPlayer) then
            
            ply:ChatPrint( "You are being hunted! " .. THE_HUNTER.HunterPlayer:Nick() .. " is the Hunter! Survive til the end of the round!" )

            ply:SetTeam( THE_HUNTER.DefaultTeam )

            player_manager.SetPlayerClass( ply, THE_HUNTER.DefaultClass )
            BaseClass.PlayerSpawn( self, ply )

        end

    end
    
    net.Start( "ghost-hunter-draw-halos" )
    net.Send( THE_HUNTER.HunterPlayer )

    CURRENT_GAME:SendGoals( { THE_HUNTER.HunterPlayer }, CURRENT_GAME.GoalAlt )

end

function THE_HUNTER:OnTimeUp()

    CURRENT_GAME:PlayerDefeat( THE_HUNTER.HunterPlayer )

    THE_HUNTER:CleanupAndEnd()

end

function THE_HUNTER:CleanupAndEnd()

    BASE:CleanupAndEnd()
    
    net.Start( "ghost-hunter-remove-halos" )
    net.Send( THE_HUNTER.HunterPlayer )

end

local function CheckPlayerCount( updatePly )

    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1

        end

    end

    if (alivePlayers <= 1) then

        CURRENT_GAME:PlayerVictory( { THE_HUNTER.HunterPlayer } )

        THE_HUNTER:CleanupAndEnd()
    
    else

        PrintMessage( 3, "There are " .. alivePlayers - 1 .. " of the hunted left!" )

    end

end

hook.Add("PlayerDeath", "THE_HUNTER:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= THE_HUNTER) then return end

    if ( victim == THE_HUNTER.HunterPlayer ) then
       
        CURRENT_GAME:PlayerDefeat( THE_HUNTER.HunterPlayer )

        THE_HUNTER:CleanupAndEnd()

    else

        CheckPlayerCount( victim )

    end

    if (attacker:IsPlayer() && victim:IsPlayer() && victim ~= attacker) then

        attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

    end
    
end)

hook.Add("PlayerDisconnected", "THE_HUNTER:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= THE_HUNTER) then return end

    if ( ply == THE_HUNTER.HunterPlayer ) then
       
        PrintMessage( 3, "The Hunter just disconnected..." )

        CURRENT_GAME:PlayerDefeat( THE_HUNTER.HunterPlayer )

        THE_HUNTER:CleanupAndEnd()

    else

        CheckPlayerCount( ply )

    end
    
end)