DEFINE_BASECLASS( "gamemode_base" )

GHOST_HUNTER = BASE:New()

GHOST_HUNTER.Name = "Ghost Hunter"
GHOST_HUNTER.Goal = "Find and kill the invisisble ghost player!"
GHOST_HUNTER.GoalAlt = "You are the ghost! You are invisible, go and hide!"
GHOST_HUNTER.DefaultClass = "player_ffa"
GHOST_HUNTER.AllowSpawning = false
GHOST_HUNTER.DamageEnabled = true
GHOST_HUNTER.DefaultTeam = 6
GHOST_HUNTER.RoundTime = 240
GHOST_HUNTER.KillPoints = 100
GHOST_HUNTER.AvailableModifiers = { LOW_GRAVITY, CHAOS_GRAVITY, HEAVY_GRAVITY, PROP_RAIN, EXPLOSIVE_PLAYERS, ZERO_GRAVITY }

GHOST_HUNTER.GhostPlayer = nil
GHOST_HUNTER.SoundLevel = 0

util.AddNetworkString( "ghost-hunters-draw-overlay" )
util.AddNetworkString( "ghost-hunters-remove-overlay" )

function GHOST_HUNTER:Start()

    BASE:Start()

    local allPlayers = player.GetAll()
    GHOST_HUNTER.GhostPlayer = allPlayers[ math.random( #allPlayers ) ]

    GHOST_HUNTER.GhostPlayer:ChatPrint( "You are the Ghost! Your are invisible but defenseless! Hide until the end of the round!" )

    GHOST_HUNTER.GhostPlayer:SetRenderMode( RENDERMODE_TRANSALPHA )
    GHOST_HUNTER.GhostPlayer:SetColor( Color( 0, 0, 0, 0 ) )

    GHOST_HUNTER.GhostPlayer:SetTeam(5)

    player_manager.SetPlayerClass( GHOST_HUNTER.GhostPlayer, "player_ghost" )
    BaseClass.PlayerSpawn( self, GHOST_HUNTER.GhostPlayer )

    net.Start( "ghost-hunters-draw-overlay" )
    net.Send( GHOST_HUNTER.GhostPlayer )

    GHOST_HUNTER.SoundLevel = 20
    timer.Create("ghost-hunter-alert-timer", 10, 0, function() 

        GHOST_HUNTER.GhostPlayer:EmitSound( "plats/elevbell1.wav", GHOST_HUNTER.SoundLevel, 100, 1 )
        GHOST_HUNTER.SoundLevel = GHOST_HUNTER.SoundLevel + 10
        if (GHOST_HUNTER.SoundLevel > 100) then

            GHOST_HUNTER.SoundLevel = 100

        end

        if (GHOST_HUNTER.SoundLevel == 55) then

            GHOST_HUNTER.SoundLevel = 60

        end


    end)

    for key, ply in pairs( player.GetAll() ) do

        if (ply ~= GHOST_HUNTER.GhostPlayer) then
            
            ply:ChatPrint( "There is a ghost on the loose! " .. GHOST_HUNTER.GhostPlayer:Nick() .. " is the Ghost! They are invisible but unarmed. Find them and kill them!" )

            ply:SetTeam(GHOST_HUNTER.DefaultTeam)

            player_manager.SetPlayerClass( ply, GHOST_HUNTER.DefaultClass )
            BaseClass.PlayerSpawn( self, ply )

        end

    end

    CURRENT_GAME:SendGoals( { GHOST_HUNTER.GhostPlayer }, CURRENT_GAME.GoalAlt )

end

function GHOST_HUNTER:OnTimeUp()

    CURRENT_GAME:PlayerVictory( { GHOST_HUNTER.GhostPlayer } )

    GHOST_HUNTER:CleanupAndEnd()

end

function GHOST_HUNTER:CleanupAndEnd()

    timer.Remove("ghost-hunter-alert-timer")

    GHOST_HUNTER.GhostPlayer:SetRenderMode( RENDERMODE_NORMAL )
    GHOST_HUNTER.GhostPlayer:SetColor( Color( 255, 255, 255, 255 ) )

    net.Start( "ghost-hunters-remove-overlay" )
    net.Send( GHOST_HUNTER.GhostPlayer )

    BASE:CleanupAndEnd()

end

local function CheckPlayerCount( updatePly )

    local alivePlayers = 0

    for key, ply in pairs( player.GetAll() ) do

        if (ply:Alive() && ply:GetObserverMode() == OBS_MODE_NONE && ply ~= updatePly) then

            alivePlayers = alivePlayers + 1

        end

    end

    if (alivePlayers <= 1) then

        CURRENT_GAME:PlayerVictory( { GHOST_HUNTER.GhostPlayer } )

        GHOST_HUNTER:CleanupAndEnd()

    end

end

hook.Add("PlayerDeath", "GHOST_HUNTER:PlayerDeath", function( victim, inflictor, attacker )

    if (CURRENT_GAME ~= GHOST_HUNTER) then return end

    if ( victim == GHOST_HUNTER.GhostPlayer ) then
       
        CURRENT_GAME:PlayerDefeat( GHOST_HUNTER.GhostPlayer )

        GHOST_HUNTER:CleanupAndEnd()

        attacker:SetNWInt( "Points", attacker:GetNWInt( "Points", 0 ) + CURRENT_GAME.KillPoints )

    else

        CheckPlayerCount( victim )

    end
    
end)

hook.Add("PlayerDisconnected", "GHOST_HUNTER:PlayerDisconnected", function( ply )

    if (CURRENT_GAME ~= GHOST_HUNTER) then return end

    if ( ply == GHOST_HUNTER.GhostPlayer ) then
       
        PrintMessage( 3, "The Ghost just disconnected... The ghost hunters win..." )

        CURRENT_GAME:PlayerDefeat( GHOST_HUNTER.GhostPlayer )

        GHOST_HUNTER:CleanupAndEnd()

    else

        CheckPlayerCount( ply )

    end
    
end)

hook.Add("PlayerCanPickupItem", "GHOST_HUNTER:PlayerCanPickupItem", function( ply )

    if (CURRENT_GAME ~= GHOST_HUNTER) then return end

    if (ply == GHOST_HUNTER.GhostPlayer) then

        return false

    else

        return true

    end
    
end)

hook.Add("PlayerCanPickupWeapon", "GHOST_HUNTER:PlayerCanPickupWeapon", function( ply )

    if (CURRENT_GAME ~= GHOST_HUNTER) then return end

    if (ply == GHOST_HUNTER.GhostPlayer) then

        return false

    else

        return true

    end
    
end)