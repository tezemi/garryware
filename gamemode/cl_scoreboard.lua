SCOREBOARD = SCOREBOARD or {}

function SCOREBOARD:Show()

    local scoreboardWidth = 500
    local scoreboardHeight = 500

	local panel = vgui.Create( "DPanel" )
    panel:SetPos( ScrW() / 2 - scoreboardWidth / 2, ScrH() / 2 - 200  )
    panel:SetSize( scoreboardWidth, scoreboardHeight )
    panel:SetBackgroundColor( Color( 0, 0, 0, 0 ) )
   
    local list = vgui.Create( "DIconLayout", panel )
    list:Dock( FILL )
    list:SetSpaceY( 5 )
    list:SetSpaceX( 5 )

    local players = player.GetAll()
    for k, v in pairs(players) do

        local listItem = list:Add( "DPanel" )
        listItem:SetSize( panel:GetWide(), 40 )

        local avatar = vgui.Create( "AvatarImage", listItem )
        avatar:SetSize( listItem:GetTall(), listItem:GetTall() )
        avatar:SetPos( 0, 0 )
        avatar:SetPlayer( v, 64 )

        if ( v:Alive() && v:GetObserverMode() == OBS_MODE_NONE ) then

            SCOREBOARD:ShowScoreboardText( listItem, 50, v:Nick(), team.GetColor( v:Team() ) )
            listItem:SetBackgroundColor( Color( 100, 100, 255 ) )

        else

            SCOREBOARD:ShowScoreboardText( listItem, 50, v:Nick() .. " ☠", Color( 255, 255, 255 ) )
            listItem:SetBackgroundColor( Color( 255, 100, 100 ) )

        end

        SCOREBOARD:ShowScoreboardText( listItem, 150, "Games Won: " .. v:GetNWInt( "GamesWon", 0 ), Color( 255, 255, 255 ) )

        SCOREBOARD:ShowScoreboardText( listItem, 325, "Points: " .. v:GetNWInt( "Points", 0 ), Color( 255, 255, 255 ) )

        local lx, ly = listItem:GetPos()
        local px, py = panel:GetPos()
        print ( ly + listItem:GetTall() .. " > " .. py + panel:GetTall() )
        if ( ly + listItem:GetTall() > py + panel:GetTall() ) then

            listItem:Remove()
            break

        end

    end

	function SCOREBOARD:Hide()

        panel:Remove()

    end
    
end

function GM:ScoreboardShow()

    SCOREBOARD:Show()
    
end

function GM:ScoreboardHide()

    SCOREBOARD:Hide()
    
end

function SCOREBOARD:ShowScoreboardText( listItem, xpos, string, color )

    local shadow = vgui.Create( "DLabel", listItem )
    shadow:SetPos( xpos + 1, listItem:GetTall() / 4.3 + 1 ) 
    shadow:SetText( string )
    shadow:SetFont( "ScoreboardDefault" )
    shadow:SizeToContents()
    shadow:SetColor( Color( 0, 0, 0 ) )

    local text = vgui.Create( "DLabel", listItem )
    text:SetPos( xpos, listItem:GetTall() / 4.3 )
    text:SetText( string )        
    text:SetFont( "ScoreboardDefault" )
    text:SizeToContents()
    text:SetColor( color )

end