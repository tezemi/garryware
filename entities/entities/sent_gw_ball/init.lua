AddCSLuaFile( "cl_init.lua" );
AddCSLuaFile( "shared.lua" );
include( 'shared.lua' );

DEFINE_BASECLASS( "base_anim" )

ENT.PrintName = "Funky Ball"
ENT.Author = "Garry Newman"
ENT.Information = "Just a bouncy ball"
ENT.Category = "Garryware"

ENT.Spawnable = true
ENT.AdminOnly = false
ENT.RenderGroup = RENDERGROUP_TRANSLUCENT

ENT.MinSize = 4
ENT.MaxSize = 128

ENT.PhysX = 1.1
ENT.PhysY = 1.1
ENT.PhysZ = 1.1
ENT.OnDamagePhysScale = 3

function ENT:SetupDataTables()

	self:NetworkVar( "Float", 0, "BallSize", { KeyName = "ballsize", Edit = { type = "Float", min = self.MinSize, max = self.MaxSize, order = 1 } } )
	self:NetworkVar( "Vector", 0, "BallColor", { KeyName = "ballcolor", Edit = { type = "VectorColor", order = 2 } } )

	self:NetworkVarNotify( "BallSize", self.OnBallSizeChanged )

end

function ENT:SpawnFunction( ply, tr, ClassName )

	if ( !tr.Hit ) then return end

	local size = math.random( 16, 48 )

	local ent = ents.Create( ClassName )
	ent:SetPos( tr.HitPos + tr.HitNormal * size )
	ent:Spawn()
	ent:Activate()

	return ent

end

function ENT:Initialize()

	self:SetModel( "models/props_phx/misc/soccerball.mdl" )
	self:SetModelScale( self:GetModelScale() * 1.25, 0 )
	self:PhysicsInit( SOLID_VPHYSICS )
	self:SetMoveType( MOVETYPE_VPHYSICS )
	self:SetSolid( SOLID_VPHYSICS )
	self:SetUseType( SIMPLE_USE )
	self:DrawShadow( false )

	local l, a = self:GetPhysicsObject():GetDamping()
	self:GetPhysicsObject():SetDamping( 0, a ) 

	self:PhysWake()	

end

local BounceSound = Sound( "garrysmod/balloon_pop_cute.wav" )

function ENT:PhysicsCollide( data, physobj )

	-- Play sound on bounce
	if ( data.Speed > 60 && data.DeltaTime > 0.2 ) then

		local pitch = 32 + 128 - math.Clamp( self:GetBallSize(), self.MinSize, self.MaxSize )
		sound.Play( BounceSound, self:GetPos(), 75, math.random( pitch - 10, pitch + 10 ), math.Clamp( data.Speed / 150, 0, 1 ) )

	end

	-- Bounce like a crazy bitch
	local LastSpeed = math.max( data.OurOldVelocity:Length(), data.Speed )
	local NewVelocity = physobj:GetVelocity()
	NewVelocity:Normalize()

	LastSpeed = math.max( NewVelocity:Length(), LastSpeed )

	local TargetVelocity = NewVelocity * LastSpeed * Vector( self.PhysX, self.PhysY, self.PhysZ )

	--[[
	if (math.abs(TargetVelocity.x) < 0.25 && math.abs(TargetVelocity.y) < 0.25) then

		TargetVelocity = TargetVelocity * Vector( math.random() * self.PhysX * 100, math.random() * self.PhysY * 100, 1 )

	end
	]]

	physobj:SetVelocity( TargetVelocity )

end

function ENT:OnTakeDamage( dmginfo )

	self:TakePhysicsDamage( dmginfo )

	local physobj = self:GetPhysicsObject()
	local NewVelocity = physobj:GetVelocity()
	local TargetVelocity = NewVelocity * self.OnDamagePhysScale

	physobj:SetVelocity( TargetVelocity )

end
