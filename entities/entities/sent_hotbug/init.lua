AddCSLuaFile( "cl_init.lua" );
AddCSLuaFile( "shared.lua" );
include( 'shared.lua' );

function ENT:Initialize()

	self:SetModel( "models/weapons/w_bugbait.mdl" );
	self:PhysicsInit( SOLID_VPHYSICS );
	self:SetMoveType( MOVETYPE_VPHYSICS );
	self:SetSolid( SOLID_VPHYSICS );
	self:SetUseType( SIMPLE_USE );
	self:DrawShadow( false );

	local phys = self:GetPhysicsObject();
	if ( phys:IsValid() ) then

		phys:Wake();
		phys:EnableDrag(true);
		phys:EnableMotion(true);

	end	

	util.SpriteTrail( self, 0, Color( 255, 20, 20 ), false, 10, 0, 0.75, 1 / ( 15 + 1 ) * 0.5, "trails/plasma" )

end

function ENT:PhysicsCollide( colData, collider )

	if (colData.HitEntity:IsPlayer()) then

		colData.HitEntity:Give( "weapon_hotbug" )
		self:Remove()

	end

end
