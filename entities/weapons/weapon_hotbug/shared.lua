AddCSLuaFile();

SWEP.PrintName			    = "Hotbug";
SWEP.Category				= "Garryware";
SWEP.ViewModel		        = "models/weapons/c_bugbait.mdl";
SWEP.WorldModel		        = "models/weapons/w_bugbait.mdl";
SWEP.UseHands				= true;
SWEP.Slot			        = 1;
SWEP.SlotPos			    = 3;
SWEP.Spawnable              = true;
SWEP.AdminOnly              = false;
SWEP.Primary.ClipSize		= -1;
SWEP.Primary.DefaultClip	= -1;
SWEP.Primary.Automatic		= false;
SWEP.Primary.Ammo		    = "";
SWEP.Secondary.ClipSize		= -1;
SWEP.Secondary.DefaultClip	= -1;
SWEP.Secondary.Ammo		    = "";

function SWEP:Initialize()

end

function SWEP:PrimaryAttack()	

	self.Weapon:SendWeaponAnim( ACT_VM_THROW )
	self:ThrowTurtle()
	timer.Simple( 0.25, function() self:Remove() end  ) 	

end

function SWEP:SecondaryAttack()

end


function SWEP:Reload()

	return false

end

function SWEP:Holster()

	return true
	
end

function SWEP:ThrowTurtle( model_file )

	if ( CLIENT ) then return end

	local ent = ents.Create( "sent_hotbug" )
	ent:SetPos( self.Owner:EyePos() + (self.Owner:GetAimVector() * 4) )
	ent:Spawn()

	local phys = ent:GetPhysicsObject()
	local vel = self.Owner:GetAimVector()
	vel = vel * 1000
	phys:ApplyForceCenter( vel )
	--ent:SetCollisionGroup( COLLISION_GROUP_PASSABLE_DOOR )

end